set mouse=a
set numberwidth=1
set clipboard=unnamedplus
syntax enable
set showcmd
set ruler
set encoding=utf-8
set showmatch
set relativenumber

set laststatus=2
set noshowmode

call plug#begin ('~/.config/nvim/plugged')

" IDE
 Plug 'easymotion/vim-easymotion'
 Plug 'scrooloose/nerdtree'
 Plug 'christoomey/vim-tmux-navigator'
 Plug 'jpalardy/vim-slime', { 'for': 'python' }
 Plug 'hanschen/vim-ipython-cell', { 'for': 'python' }

 " Autocomplete
 Plug 'neoclide/coc.nvim', {'branch': 'release'}

" Status Bar
 Plug 'maximbaz/lightline-ale'
 Plug 'itchyny/lightline.vim'

 " Themes
 Plug 'fxn/vim-monochrome'
 call plug#end()

 "mappins
 let mapleader = " "

 "set theme
colorscheme monochrome

let g:lightline = {
	\ 'colorscheme': 'nord',
	\}

"NerdTree
nnoremap <leader>n :NERDTreeFocus<CR>
nnoremap <C-n> :NERDTree<CR>
nnoremap <leader>t :NERDTreeToggle<CR>
nnoremap <C-f> :NERDTreeFind<CR>
